import './contacts.css';

import React, { Component } from 'react';
import ExpandableList from '../helperFunctions/expandableList';
import SimpleForm from '../helperFunctions/simpleForm';
import PopupForm from '../helperFunctions/popupForm';
import { Button, Space } from 'antd';

import { getAllContacts, createContact, deleteContact, editContact } from '../helperFunctions/dataFetchersAndSetters';

class Contacts extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        this.addContact = this.addContact.bind(this);
        this.removeContact = this.removeContact.bind(this);
        this.editContact = this.editContact.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
    }

    getContacts = async () => {
        const contactsRaw = await getAllContacts();
        let contacts = contactsRaw.map((contact) => { return {...contact, key: contact.id} });
        this.setState({
            contacts
        });
    };

    addContact = async (contactToAdd) => {
        await createContact(contactToAdd.contact)
            .then(res => {
                const newContact = {...res, key: res.id};
                let contacts = [...this.state.contacts];
                contacts.push(newContact);
                this.setState({ contacts });
            })
            .catch(err => console.log(err));
    }

    removeContact = async (contactToRemove) => {
        await deleteContact(contactToRemove)
            .then(res => {
                const contacts = this.state.contacts.filter((value, index, arr) => {
                    return !(value === contactToRemove);
                });
                this.setState({ contacts });
            })
            .catch(err => console.log(err));
    }

    editContact = async (editedContact) => {
        await editContact(editedContact)
            .then(res => {
                var idOfEdited = editedContact.id;
                var uneditedContact = this.state.contacts.filter((value, index, arr) => {
                    return (value.id === idOfEdited);
                });
                var indexOfUnedited = this.state.contacts.indexOf(uneditedContact[0]);
                let contacts = this.state.contacts;
                let editedContactWithKey = {...editedContact, key: idOfEdited };
                contacts[indexOfUnedited] = editedContactWithKey;
                this.setState({ modalVisible: false, contacts });
            })
            .catch(err => console.log(err));
    }

    editForm = async (contactToEdit) => {
        this.setState({ 
            modalVisible: true, 
            contactToEdit
        });
    };

    cancelEdit = async () => {
        this.setState({
            modalVisible: false
        });
    };

    componentDidMount() {
        this.getContacts();
    }

    makeColumn = (name) => {
        return (
            {   
                title: name.charAt(0).toUpperCase() + name.slice(1),
                dataIndex: name,
                key: name 
            }
        )
    };

    render() {
        const colNames = ['name', 'address', 'age', 'email', 'phone'];
        let columns = colNames.map(n => this.makeColumn(n));
        columns = columns.concat([{
            title: '',
            dataIndex: '',
            key: 'edit',
            render: (text, record) =>                 
                <Space size="middle">
                    <Button onClick={() => {this.editForm(record)}}>Edit</Button>
                    <Button onClick={() => this.removeContact(record)}>Delete</Button> 
                </Space>,
          }]);
        if (!this.state.contacts) {
            return(
                <div className="text-center">
                    Loading contacts...
                </div>
            );
        };

        return(
            <div>
                {   
                    this.state.modalVisible ?
                    <PopupForm 
                        modalVisible={this.state.modalVisible} 
                        editContact={this.editContact} 
                        cancelEdit={this.cancelEdit} 
                        contactToEdit={this.state.contactToEdit}
                    /> :
                    null
                }
                {
                    this.state.modalVisible 
                    ?   <ExpandableList columns={columns} data={[]} />     
                    :   <ExpandableList columns={columns} default={true} data={this.state.contacts} />    
                }                
                <div className="flex-container">
                    <div className="add-border">
                        <h1 className="center-hori">
                            Add contact
                        </h1>
                        <br/>
                        <SimpleForm  addContact={this.addContact}/>
                    </div>
                </div>
            </div>
        )
    };
};

export default Contacts;