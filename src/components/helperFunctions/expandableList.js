import React, { Component } from 'react';
import { Table } from 'antd';

class ExpandableList extends Component {
    
    constructor(props)
    {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <>
                {
                    <Table
                        columns={this.props.columns}
                        expandable={{
                        expandedRowRender: record => <p style={{ margin: 0 }}>{record.details}</p>,
                        rowExpandable: record => record.name !== 'Not Expandable',
                        }}
                        dataSource={this.props.data}
                    />
                }
            </>
        );    
    }
}
  

export default ExpandableList;