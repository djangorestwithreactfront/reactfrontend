import React from 'react';
import { Form, Input, InputNumber, Button } from 'antd';

function SimpleForm(props) {

  const [form] = Form.useForm();

  const onFinish = values => {
    props.addContact ? props.addContact(values) : props.editContact(values);
  };

  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };
  
  const validateMessages = {
    required: '${label} is required!',
    types: {
      email: '${label} is not a valid email!',
      number: '${label} is not a valid number!',
    },
    number: {
      range: '${label} must be between ${min} and ${max}',
    },
  };
  return (
    <Form 
      {...layout} 
      form={form}
      name="nest-messages" 
      onFinish={onFinish} 
      validateMessages={validateMessages} 
      onChange={() => {
        if (props.contactToEdit) {
          const id = props.contactToEdit.id;
          let editedContact = { id, ...form.getFieldsValue().contact };
          props.updateContact(editedContact);
        }
      }}
    >
      <Form.Item name={['contact', 'name']} label="Name" rules={[{ }]} initialValue={props.contactToEdit ? props.contactToEdit.name : ""}>
        <Input />
      </Form.Item>
      <Form.Item name={['contact', 'address']} label="Address" rules={[{ }]} initialValue={props.contactToEdit ? props.contactToEdit.address : ""}>
        <Input />
      </Form.Item>
      <Form.Item name={['contact', 'age']} label="Age" rules={[{ type: 'number', min: 0, max: 130 }]} initialValue={props.contactToEdit ? props.contactToEdit.age : null} >
        <InputNumber />
      </Form.Item>
      <Form.Item name={['contact', 'email']} label="Email" rules={[{ }]} initialValue={props.contactToEdit ? props.contactToEdit.email : ""}>
        <Input />
      </Form.Item>
      <Form.Item name={['contact', 'phone']} label="Phone" rules={[{ type: 'number', min: 1, max: 99999999 }]} initialValue={props.contactToEdit ? props.contactToEdit.phone : null} >
        <InputNumber />
      </Form.Item>
      <Form.Item name={['contact', 'details']} label="Details" initialValue={props.contactToEdit ? props.contactToEdit.details : ""}>
        <Input.TextArea />
      </Form.Item>
      <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }} style={{ display: props.contactToEdit ? 'none' : 'block' }}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

export default SimpleForm;
