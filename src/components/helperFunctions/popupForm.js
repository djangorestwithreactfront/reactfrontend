import React, { Component } from 'react';
import { Modal } from 'antd';
import SimpleForm from '../helperFunctions/simpleForm';

class PopupForm extends Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.updateContact = this.updateContact.bind(this);
    }

    updateContact = (editedContact) => {
        this.setState({
            editedContact
        });
    };

  render() {
    return (
    <>
        <Modal
        title="Edit"
        style={{ top: 50 }}
        visible={this.props.modalVisible}
        onOk={() => 
            {
                this.state.editedContact && this.props.editContact(this.state.editedContact);
            }
        }
        onCancel={() => this.props.cancelEdit()}
        >
            <SimpleForm contactToEdit={this.props.contactToEdit} editContact={this.props.editContact} updateContact={this.updateContact} />
        </Modal>
        <br />
        <br />
    </>
    );
  }
}

export default PopupForm;
