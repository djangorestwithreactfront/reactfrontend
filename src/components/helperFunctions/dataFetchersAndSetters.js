import axios from 'axios';

export const getAllContacts = async () => {
    const endpointContacts = process.env.REACT_APP_API + process.env.REACT_APP_ALL_CONTACTS;
    let contacts = await axios.get(endpointContacts)
        .then((data) => data.data)
        .catch((err) => console.log(err));
    
    return contacts;
};

export const createContact = async (contactToSend) => {
    const endpointContacts = process.env.REACT_APP_API + process.env.REACT_APP_ALL_CONTACTS;
    let newContact = await axios.post(endpointContacts, contactToSend)
        .then((data) => data.data)
        .catch((err) => console.log(err));
    
    return newContact;
}

export const deleteContact = async (contactToDelete) => {
    const endpointContacts = process.env.REACT_APP_API + process.env.REACT_APP_ALL_CONTACTS + contactToDelete.id;
    let deletedContact = await axios.delete(endpointContacts)
        .then((data) => data.data)
        .catch((err) => console.log(err));
    
    return deletedContact;
}

export const editContact = async (editedContact) => {
    const endpointContacts = process.env.REACT_APP_API + process.env.REACT_APP_ALL_CONTACTS + editedContact.id;
    let returnedEditedContact = await axios.put(endpointContacts, editedContact)
        .then((data) => data.data)
        .catch((err) => console.log(err));
    
    return returnedEditedContact;
}