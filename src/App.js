import './App.css';
import 'antd/dist/antd.css';

import Contacts from './components/contacts/contacts';
import { BrowserRouter, Switch, Route } from 'react-router-dom';


function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={ Contacts }/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
