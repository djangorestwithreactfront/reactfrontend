# React frontend contact list
The frontend of a simple contact list, where one can do CRUD operations

This frontend is meant to be used with the corresponding Django backend, which can be found [here](https://gitlab.com/djangorestwithreactfront/djangobackend)

# Getting started

## Development mode
* Clone this repository
* Run npm i
* Run npm start


# Prerequisites
Node


# Authors
* Jens CT Hetland